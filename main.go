package main

import (
	"flag"
	"gitlab.com/domio-art/moneyconv/conv"
	"fmt"
)

func main() {
	var rate float64
	var dstType string
	var srcMoney conv.Money
	
	flag.Float64Var(&srcMoney.Amount, "amount", 0.0, "количество денег которое нужно конвертировать по курсу")
	flag.Float64Var(&rate, "rate", 0.0, "курс по которому конвертируются деньги")
	flag.StringVar(&srcMoney.Type, "src", "RUB", "код входящей валюты")
	flag.StringVar(&dstType, "dst", "USD", "код исходящей валюты")
	flag.Parse()
	
	dstMoney := conv.Conv(&srcMoney, dstType, rate)
	fmt.Printf("%.2f %s = %.2f %s\n", srcMoney.Amount, srcMoney.Type, dstMoney.Amount, dstMoney.Type)
}

