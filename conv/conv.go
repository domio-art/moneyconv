package conv

func Conv(src *Money, dstType string, rate float64) Money {
	return Money{Amount: src.Amount * rate, Type: dstType}	
}
